# README #

Gets documents from solr-client and topicIds with emails where notificationTime bigger than 0 from pgAdmin and compares them by topicId, then publishes messages with topicId* and email* to email-send-q

# HOW TO RUN #

```
#!java

java -jar PATH-TO-JAR
```

*PATH-TO-JAR* - path, where jar file is located + filename. For example if jar file is in current directory, it is just "email-notification-service.jar"


EXAMPLE:

```
#!java

java -jar email-notification-service.jar
```

# DESCRIPTION #

Service consumes messages from **email-notification-q**. One message is JSON object - of the type Document. Service takes following fields from Document:

* *mediaType* - type of the media, by which messages need to be filtered

* *topicId* - id of the topic, which need to be compared with topicId from data base and added in the text of message

* *title* - title of the topic, which need to be added in the text of message

* *url* - url of the topic, which need to be added in the text of message

* *highlights* - highlights of the topic, which need to be added in the text of message

If the mediaType is not **"social"** and topicId of Document equals with topicId from data base, the system creates the set of models of type Note. Model contains fields *title* , *url*,  *highlights*  from Document and *email* - email address of client, for which message need to be sent.

After generation Note, system publishes Note as JSON object to **email-send-q**.

