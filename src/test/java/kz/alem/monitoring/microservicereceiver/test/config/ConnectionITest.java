/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.alem.monitoring.microservicereceiver.test.config;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.impl.DefaultExceptionHandler;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import junit.framework.Assert;
import kz.alem.monitoring.email.notification.service.config.AppConfig;
import org.junit.Test;
import org.apache.log4j.Logger;

public class ConnectionITest {

    private final Logger log = Logger.getLogger(ConnectionITest.class);

    AppConfig appConfig;

    @Test
    public void connectionPGClientTest() {
        try {
            appConfig = new AppConfig();
            Class.forName(appConfig.getClassName());
            Connection con = DriverManager.getConnection(appConfig.getUrl(), appConfig.getUser(), appConfig.getPass());
            Assert.assertEquals(con.isClosed(), false);
            con.close();
        } catch (SQLException ex) {
           log.error("Connection can not be created: ", ex);
        } catch (IOException ex) {
            log.error("AppConfig can not be created: ", ex);
        } catch (ClassNotFoundException ex) {
            log.error("Cannot get className from appConfig ", ex);
        }
    }

    @Test
    public void connectionRabbitClientTest() {

        try {
            appConfig = new AppConfig();
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(appConfig.getRabbitHost());
            factory.setPassword(appConfig.getRabbitPassword());
            factory.setUsername(appConfig.getRabbitUser());
            factory.setConnectionTimeout(30000);
            factory.setAutomaticRecoveryEnabled(true);
            factory.setTopologyRecoveryEnabled(true);
            factory.setNetworkRecoveryInterval(10000);
            factory.setExceptionHandler(new DefaultExceptionHandler());
            factory.setRequestedHeartbeat(360);
            com.rabbitmq.client.Connection connection = factory.newConnection();
            Assert.assertEquals(connection.isOpen(), true);
            connection.close();
        } catch (TimeoutException | IOException ex) {
            log.error("EXCEPTION: ", ex);
        }
    }

}
