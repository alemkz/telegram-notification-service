package kz.alem.monitoring.microservicereceiver.test.date;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import junit.framework.Assert;
import org.junit.Test;

public class DateTest {

    @Test
    public void gerDifferenceBetweenDates() throws ParseException {
        String jdate1 = "2016-12-03T10:05:03Z";
        String jdate2 = "2017-01-15T10:05:03Z";
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date dateOfDocument = format.parse(jdate2);
        int days = (int) ((System.currentTimeMillis() - dateOfDocument.getTime()) / (1000 * 60 * 60 * 24));
        if (days >= 2) {
            System.out.println("cunsuming ... " + days);
        }
        Assert.assertTrue(((int) ((format.parse(jdate1).getTime() - format.parse(jdate2).getTime()) / (1000 * 60 * 60 * 24))) != 2);
        SimpleDateFormat format2 = new SimpleDateFormat("MMM d, yyyy h:mm:ss a");

        Date dateOfDocument2 = format2.parse("Jan 20, 2017 5:08:00 PM");
        dateOfDocument2.setTime(dateOfDocument2.getTime() - (1000 * 6 * 60 * 60));
        System.out.println("Updated date: " + dateOfDocument2);

        SimpleDateFormat format3 = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        System.out.println(format3.format(dateOfDocument2));

        SimpleDateFormat format4 = new SimpleDateFormat("dd MMM yyyy h:mm");
        System.out.println(format4.format(dateOfDocument2));

    }

}
