/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.alem.monitoring.microservicereceiver.test.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import kz.alem.monitoring.email.notification.service.model.Notification;
import org.hibernate.validator.constraints.Email;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NotificationTest {

    private List<String> highlight;
    private String url;
    private String title;

    @Before
    public void initialization() {
        url = "https://mail.ru/";
        title = "tesTitle";
        highlight = new ArrayList<>();
        String highlihgt = "this is test";
        String highlihgt2 = "Hello World";
        String highlihgt3 = "this is test";
        highlight.add(highlihgt);
        highlight.add(highlihgt2);
        highlight.add(highlihgt3);
    }

    @Test
    public void test() {
        HashMap<String, List<String>> urls = new HashMap<>();
        Notification note = new Notification();
        note.setDomain("mail.ru");
        List<String> emails = new ArrayList<>();
        emails.add("ainura");
        note.setEmails(emails);
        note.setTitle("TestTitle");
        urls.put(note.getDomain() + " " + note.getTitle().trim(), emails);
        
        emails = new ArrayList<>();
        emails.add("sss");
        emails.add("kkk");
        if (urls.isEmpty()) {
            urls.put(note.getDomain() + " " + note.getTitle().trim(), emails);
        } else {
            for (String e : emails) {
                urls.get(note.getDomain() + " " + note.getTitle().trim()).add(e);
            }
        }
        System.out.println(urls.get(note.getDomain() + " " + note.getTitle().trim()).size());
        Assert.assertEquals(1, urls.size());
        
        note.setEmails(emails);
        boolean copy = false;
        
        if (!urls.isEmpty() && urls.containsKey(note.getDomain() + " " + note.getTitle().trim())) {
            List<String> emailss = urls.get(note.getDomain() + " " + note.getTitle().trim());
              copy = !Collections.disjoint(emailss, note.getEmails());
        }
        Assert.assertEquals(true, copy);
        if (copy == false) {
            System.out.println("email will be sent");
        }
        Assert.assertEquals("tesTitle", title);

    }

}
