package kz.alem.monitoring.microservicereceiver.test.client;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import junit.framework.Assert;
import kz.alem.monitoring.email.notification.service.model.Topic;
import kz.alem.monitoring.email.notification.service.pg.PGClient;
import org.junit.Test;
import org.apache.log4j.Logger;

public class PGClientIT {

    private final Logger log = Logger.getLogger(PGClientIT.class);

    @Test
    public void getEmailsAndTopics()  {
        try {
            PGClient client = new PGClient();
            List<Topic> topics = client.getTopics();
            topics.forEach((topic) -> {
                log.info(topic.getTopicId() + " " + topic.getEmails() +" "+topic.getGeoposotion());
            });
            Assert.assertNotNull(topics.size());
        } catch (ClassNotFoundException | SQLException | IOException ex) {
            log.error("Exception: ", ex);
        }

    }
}
