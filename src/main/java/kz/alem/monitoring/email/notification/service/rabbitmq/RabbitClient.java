package kz.alem.monitoring.email.notification.service.rabbitmq;

import kz.alem.monitoring.email.notification.service.model.Notification;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.MessageProperties;
import com.rabbitmq.client.impl.DefaultExceptionHandler;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeoutException;
import jdk.nashorn.internal.parser.JSONParser;
import kz.alem.monitoring.email.notification.service.config.AppConfig;
import kz.alem.monitoring.email.notification.service.model.Topic;
import kz.alem.monitoring.email.notification.service.pg.PGClient;
import org.apache.log4j.Logger;

/**
 * Class that response to connection to rabbit and getting message as Document
 * then convert it to Notification (If the document meets the requirements from
 * pg) and publish it to email-send-queue
 *
 * @author ainur
 */
public class RabbitClient {

    private final Logger log = Logger.getLogger(RabbitClient.class);

    private Channel channel = null;
    private Connection connection = null;
    private final String inputQueueName;
    private final String inputExchangeName;
    private final String inputBindingKey;
    private final String outputQueueName;
    private final String outputExchangeName;
    private List<Topic> topics;
    private Date startDate;
    private Date startDateOfSaving; //for KaspiBank
    private final Gson gson = new Gson();
    private final int UPDATE_TIME = 60 * 60 * 1000;
    private final int UPDATE_TIME_SAVING = 24 * 60 * 60 * 1000; //for KaspiBank
    private HashMap<String, ArrayList<String>> urls; //for KaspiBank

    public RabbitClient() throws IOException, TimeoutException {
        AppConfig appConfig = new AppConfig();

        inputQueueName = appConfig.getInputQueueName();
        inputExchangeName = appConfig.getInputExchangeName();
        outputQueueName = appConfig.getOutputQueueName();
        outputExchangeName = appConfig.getOutputExchangeName();
        inputBindingKey = appConfig.getInputBindingKey();
        System.out.println(inputQueueName + " " + appConfig.getRabbitHost());
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(appConfig.getRabbitHost());
        factory.setPassword(appConfig.getRabbitPassword());
        factory.setUsername(appConfig.getRabbitUser());
        factory.setConnectionTimeout(30000);
        factory.setAutomaticRecoveryEnabled(true);
        factory.setTopologyRecoveryEnabled(true);
        factory.setNetworkRecoveryInterval(10000);
        factory.setExceptionHandler(new DefaultExceptionHandler());
        factory.setRequestedHeartbeat(360);

        try {
            connection = factory.newConnection();
            channel = connection.createChannel();
            log.info("CONNECTION CREATED");
            channel.queueDeclare(inputQueueName, true, false, false, null);
            channel.queueBind(inputQueueName, inputExchangeName, inputBindingKey);

            channel.exchangeDeclare(outputExchangeName, "direct", true);
            channel.queueDeclare(outputQueueName, true, false, false, null);
            channel.queueBind(outputQueueName, outputExchangeName, outputQueueName);

            channel.basicQos(100);
            channel.addShutdownListener(null);
            startDate = new Date();

            //for KaspiBank
            startDateOfSaving = new Date();
            urls = new HashMap<>();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void consume(final PGClient pgclient) throws IOException {
        channel.basicConsume(inputQueueName, false,
                new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag,
                    Envelope envelope,
                    AMQP.BasicProperties properties,
                    byte[] body) {
                try {
                    if (topics == null || (topics != null && System.currentTimeMillis() - startDate.getTime() > UPDATE_TIME)) {
                        topics = pgclient.getTopics();
                        startDate = new Date();
                    }
                    //for KaspiBank
                    if (UPDATE_TIME_SAVING < System.currentTimeMillis() - startDateOfSaving.getTime()) {
                        log.info("URLS LAST HASH CODE " + urls.hashCode());
                        startDateOfSaving = new Date();
                        urls = new HashMap<>();
                    }

                    long deliveryTag = envelope.getDeliveryTag();
                    String rawBody =  new String(body, "UTF-8");
                    //String rwBody2  = "{\"resourceSetIds\": [3], \"categoryId\": 0, \"solrAdress\": null, \"solrAlias\": \"alem\", \"resourceSetId\": 3, \"keywords\": [\"Қазақстан*\", \"казахстан*\"], \"document\": {\"status\": \"active\", \"domain\": \"365info.kz\", \"attachments\": [{\"thumb\": null, \"title\": null, \"text\": null, \"link\": \"https://static.365info.kz/uploads/2018/04/bekker-kchs-700x468.jpg\", \"type\": \"photo\", \"id\": null}], \"links\": null, \"topicIdList\": null, \"text\": \"9 апреля на брифинге в Комитете по ЧС подведены предварительные итоги противопожарных проверок объектов торговли высокого риска. \\n\\nПроверки стартовали 2 апреля по поручению генеральной прокуратуры. На 6 апреля открыто 308 проверок, 75 завершено, сообщает пресс-служба комитета. \\n\\nНа проверенных объектах выявлено 1037 нарушений. Не выявлено нарушений всего на трех объектах — двух рынках в Шахтинске и торговом доме «Делюкс» в Карабалыкском районе Костанайской области. \\n\\nПожар в Кемерово: торговый центр построили без разрешения \\n\\n \\n\\nПроверки проходят с привлечением представителей общественности, прокуратуры и антикоррупционной службы. Списки объектов, эксплуатирующихся с нарушениями, будут размещаться на официальном сайте Комитета по ЧС. \\n\\nПо данным inform.kz со ссылкой на главу комитета Владимира Беккера, наиболее распространенными оказались следующие нарушения: захламление путей эвакуации; ненадлежащее состояние средств оповещения при пожаре; отсутствие необходимых средств обеспечения безопасности. \\n\\n«Сколько времени прошло с момента трагедии, мы начали проверки. Собственники, по сути, не реагируют. Можно же было подготовиться, понятно, что к ним придут, — \\n\\nцитирует Беккера tengrinews.kz. — \\n\\nНарушения, касающиеся эвакуационных выходов, пожарной автоматики можно было в срочном порядке устранить. К сожалению, реакция не совсем положительная у наших бизнесменов. Почему-то не реагируют на эти вещи. Нас это, конечно, возмущает». \\n\\n25 марта в Кемерово в торгово-развлекательном центре «Зимняя вишня» произошел пожар. Погибли 64 человека, в том числе 41 ребенок. \\n\\nВ. Беккер. Фото: пресс-служба КЧС\\n\", \"pid\": null, \"mediaScale\": \"central\", \"mediaPlaceLocationId\": \"424311521\", \"coverage\": 16, \"dislikesCount\": null, \"id\": \"https://365info.kz/2018/04/svyshe-1-000-pozharnyh-narushenij-vyyavleno-na-rynkah-i-tts-kazahstana/#9483ef92-4808-460b-bdda-94c11db2d1f9\", \"repost\": null, \"group\": null, \"manualSentiment\": null, \"sentiment\": \"NEUTRAL\", \"author\": \"\", \"mediaType\": \"news_agency\", \"subjectId\": \"0e04fa1b704a518e4b84bd893f92d03b\", \"children\": null, \"score\": null, \"mediaPlaceLocationCityId\": null, \"parentId\": null, \"manualGeolocation\": null, \"type\": \"article\", \"location\": null, \"profile\": null, \"topicId\": \"9483ef92-4808-460b-bdda-94c11db2d1f9\", \"terms\": null, \"titleLemmas\": \"свыше 1 000 пожарный нарушение выявлять на рынок и тц казахстан\", \"tags\": null, \"newsbreakIdList\": null, \"signature\": \"63ff42de6890b9c1dcabf82372fdf139\", \"host\": \"365info.kz\", \"birthday\": null, \"sharesCount\": null, \"date\": \"Apr 9, 2018 8:57:00 PM\", \"autoHighlight\": \"Свыше 1 000 пожарных нарушений выявлено на рынках и ТЦ <b>Казахстана</b>. 9 апреля на брифинге в Комитете по ЧС подведены предварительные итоги противопожарных проверок объектов торговли высокого риска. \\n\\nПроверки стартовали 2 апреля по поручению генеральной прокуратуры. На 6 апреля открыто 308 проверок, 75 завершено, сообщает пресс-служба комитета. \\n\\nНа проверенных объектах выявлено 1037 нарушений. Не выявлено нарушений всего на трех объектах — двух рынках в Шахтинске и торговом доме «Делюкс» в Карабалыкском районе Костанайской области. \\n\\nПожар в Кемерово: торговый центр построили без разрешения \\n\\n \\n\\nПроверки проходят с привлечением представителей общественности, прокуратуры и антикоррупционной службы. Списки объектов, эксплуатирующихся с нарушениями, будут размещаться на официальном сайте Комитета по ЧС. \\n\\nПо данным inform.kz со ссылкой на главу комитета Владимира Беккера, наиболее распространенными оказались следующие нарушения: захламление путей эвакуации; ненадлежащее состояние средств оповещения при пожаре; отсутствие необходимых средств обеспечения безопасности. \\n\\n«Сколько времени прошло с момента трагедии, мы начали проверки. Собственники, по сути, не реагируют. Можно же было подготовиться, понятно, что к ним придут, — \\n\\nцитирует Беккера tengrinews.kz. — \\n\\nНарушения, касающиеся эвакуационных выходов, пожарной автоматики можно было в срочном порядке устранить. К сожалению, реакция не совсем положительная у наших бизнесменов. Почему-то не реагируют на эти вещи. Нас это, конечно, возмущает». \\n\\n25 марта в Кемерово в торгово-развлекательном центре «Зимняя вишня» произошел пожар. Погибли 64 человека, в том числе 41 ребенок. \\n\\nВ. Беккер. Фото: пресс-служба КЧС\\n\", \"categories\": null, \"distance\": 0.107420325, \"verificationDate\": null, \"language\": \"ru\", \"created\": \"Apr 9, 2018 9:09:46 PM\", \"url\": \"https://365info.kz/2018/04/svyshe-1-000-pozharnyh-narushenij-vyyavleno-na-rynkah-i-tts-kazahstana/\", \"gender\": null, \"title\": \"Свыше 1 000 пожарных нарушений выявлено на рынках и ТЦ Казахстана\", \"geoposition\": null, \"modified\": \"Apr 9, 2018 9:09:46 PM\", \"mediaPlaceLocationCoordinates\": null, \"places\": null, \"published\": \"Apr 9, 2018 8:57:00 PM\", \"textLemmas\": \"9 апрель на брифинг в комитет по чс подводить предварительный итог противопожарный проверка объект торговли высокого риск проверка стартовать 2 апрель по поручение генеральный прокуратура на 6 апрель открыто 308 проверка 75 завершать сообщать пресс-служба комитет на проверять объект выявлять 1037 нарушение не выявлять нарушение всего на три объект — двух рынок в шахтинске и торговый доме «делюкс» в карабалыкском район костанайской область пожар в кемерово торговый центр построить без разрешение проверка проходить с привлечение представитель общественности, прокуратура и антикоррупционной служба список объект эксплуатирующихся с нарушение будут размещаться на официальный сайт комитет по чс. по данные inform.kz со ссылка на глава комитет владимир беккер наиболее распространять оказываться следующий нарушение захламление путь эвакуация ненадлежащий состояние средство оповещение при пожар отсутствие необходимый средство обеспечение безопасность «сколько время проходить с момент трагедия мы начали проверка собственник по сути, не реагировать можно же было подготавливаться понятно что к ним прийти — цитировать беккер tengrinews.kz. — нарушение касающиеся эвакуационный выход пожарный автоматика можно было в срочный порядок устранять к сожалению, реакция не совсем положительный у наших бизнесмен почему-то не реагировать на эти вещь нас это, конечно, возмущать 25 март в кемерово в торгово-развлекательном центр зимний вишня происходить пожар погибать 64 человек в то числе 41 ребенок в беккер фото пресс-служба кчс\", \"highlight\": [\"Свыше 1 000 пожарных нарушений выявлено на рынках и ТЦ <b>Казахстана</b>. 9 апреля на брифинге в Комитете по ЧС подведены предварительные итоги противопожарных проверок объектов торговли высокого риска.,0\", \"Проверки стартовали 2 апреля по поручению генеральной прокуратуры.,1\", \"_big_\"], \"verificationStatus\": null, \"commentsCount\": null, \"likesCount\": null}, \"geoposition\": \"\"}";
                    JsonElement  obj  = gson.fromJson(rawBody, JsonElement.class);
                   
                    String mediaType = "undefined";
                    String topicId = null;
                    String domain = "";
                    String geoposition = "";
                    // Jan 18, 2017 6:09:51 PM  yyyy-MM-dd'T'HH:mm:ss'Z' 2017-01-19T00:00:00Z MMM d, yyyy h:mm:ss a
                    DateFormat format = new SimpleDateFormat("MMM d, yyyy h:mm:ss a", Locale.ENGLISH);
                    Date publishedDateOfDocument = null;
                    Date createdDateOfDocument = new Date();
                    JsonObject document = null;
                    try {
                        document =   obj.getAsJsonObject().
                                get("document").
                                getAsJsonObject();

                        topicId = document.get("topicId").getAsString();
                        domain = document.get("domain").getAsString();
                    } catch (NullPointerException ex) {
                        ex.printStackTrace();
                    }
                    try {
                        mediaType = document.get("mediaType").getAsString();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        mediaType = "undefined";
                        log.info(domain + " " + document.get("id").getAsString());
                    }
                    if (mediaType != null && "social".equals(mediaType.trim())) {
                        mediaType = domain.trim().substring(0, domain.indexOf("."));
                    }
                    log.info("TOPIC_ID:" + topicId + ", media:" + mediaType + ",domain:" + domain);

                    for (Topic topic : topics) {
                        if (topic.getTopicId().equals(topicId) && topic.getMediaTypes().contains(mediaType.trim())) {
                            log.info("=========================   Note ==================================");
                            String title = "";
                            String url = "";
                            String dateStringPublished;
                            String dateStringCreated;
                            String sentiment = "";
                            int days = 0;
                            log.info("Domain and mediaType: " + domain + " " + mediaType);
                            try {
                                title = document.getAsJsonObject().get("title").getAsString();
                            } catch (Exception ex) {
                                log.error("EXCEPTION: Title can not retrieved from JsonElement " + ex);
                            }
                            try {
                                url = document.get("url").getAsString();
                            } catch (Exception ex) {
                                log.error("EXCEPTION: Url can not retrieved from JsonElement " + ex);
                            }
                            try {
                                sentiment = document.get("sentiment").getAsString();
                            } catch (Exception ex) {
                                log.error("EXCEPTION: Sentiment or Date can not retrieved from JsonElement " + ex);
                            }
                            try {
                                geoposition = document.get("geoposition").getAsString();
                            } catch (Exception ex) {
                            }
                            try {
                                if (document.get("published").getAsString() != null) {
                                    dateStringPublished = document.get("published").getAsString().replace("\"", "");
                                    //log.info("Published Date of json object: " + dateStringPublished);
                                    publishedDateOfDocument = format.parse(dateStringPublished);
                                    publishedDateOfDocument.setTime(publishedDateOfDocument.getTime() - (1000 * 6 * 60 * 60));
                                    log.info("Parsed published date : " + publishedDateOfDocument);
                                }
                                dateStringCreated = document.get("created").getAsString();
                                dateStringCreated = dateStringCreated.replace("\"", "");
                                //log.info("Created Date of json object: " + dateStringCreated);
                                createdDateOfDocument = format.parse(dateStringCreated);
                                createdDateOfDocument.setTime(createdDateOfDocument.getTime() - (1000 * 6 * 60 * 60));
                                log.info("Parsed created date : " + createdDateOfDocument);
                            } catch (Exception ex) {
                                log.error("EXCEPTION: Date can not retrieved from JsonElement " + ex);
                            }
                            if (publishedDateOfDocument != null) {
                                days = (int) ((System.currentTimeMillis() - publishedDateOfDocument.getTime()) / (1000 * 60 * 60 * 24));
                            } else {
                                days = (int) ((System.currentTimeMillis() - createdDateOfDocument.getTime()) / (1000 * 60 * 60 * 24));
                            }
                            //log.info(days);
                            JsonArray arr = document.get("highlight").getAsJsonArray();
                            List<String> highlight = new ArrayList<>();
                            for (JsonElement el : arr) {
                                highlight.add(el.getAsString());
                            }
                            Notification note = new Notification();
                           // note.setEmails(topic.getEmails());
                           note.setChats(topic.getChats());
                            if (url.endsWith("/")) {
                                note.setUrl(url.substring(0, url.lastIndexOf("/")));
                            } else {
                                note.setUrl(url);
                            }
                            note.setTime(topic.getTime());
                            note.setTopicName(topic.getTopicName());
                            note.setTopicId(topic.getTopicId());
                            note.setTitle(title);
                            note.setHighlight(highlight);
                            note.setDomain(domain);
                            note.setDatePublished(publishedDateOfDocument);
                            note.setDateCreated(createdDateOfDocument);
                            note.setSentiment(sentiment.trim());
                            note.setMediaType(mediaType.trim());
                            note.setScope(topic.getScope());
                            String restrictedUrl = "fininfo.kz/iris/news/";
                            boolean copy = false;
                            boolean restriction = note.getUrl().contains(restrictedUrl);
                            boolean geo = (geoposition.equals(topic.getGeoposotion())
                                    || geoposition.equals("") || topic.getGeoposotion().equals("")) ? true : false;
                            if (!urls.isEmpty() && urls.containsKey(note.getDomain() + " " + note.getTitle().trim())) {
                              //  copy = !Collections.disjoint(urls.get(note.getDomain() + " " + note.getTitle().trim()), note.getEmails() );
                              copy = !Collections.disjoint(urls.get(note.getDomain() + " " + note.getTitle().trim()), note.getChats());
                            }
                            if (!title.equals("") && !url.equals("") && !domain.equals("") && (days < 2) && restriction == false && copy == false
                                    && topic.getSentiments().contains(note.getSentiment().toLowerCase()) && geo == true) {
                                //log.info("Notification topicId, emails and url: " + note.getTopicId() + " " + note.getEmails() + " " + note.getUrl());
                                log.info("Notification topicId, emails and url: " + note.getTopicId() + " " + note.getChats()+ " " + note.getUrl());
                                log.info("Notification mediaType and sentiment: " + note.getMediaType() + " " + note.getSentiment());
                                log.info("Notification created and published dates: " + note.getDateCreated() + " " + note.getDatePublished());
                                log.info("Notification domain and title: " + note.getDomain() + " " + note.getTitle().trim());
                                if (urls.isEmpty() || !urls.containsKey(note.getDomain() + " " + note.getTitle().trim())) {
                                   // ArrayList<String> emails = new ArrayList<>(note.getEmails());
                                   ArrayList<String> chats = new ArrayList<>(note.getChats());
                                    urls.put(note.getDomain() + " " + note.getTitle().trim(), chats);
                                } else {
                                 //   for (String email : note.getEmails()) {
                                 for (String chat : note.getChats()) {
                                     urls.get(note.getDomain() + " " + note.getTitle().trim()).add(chat);
                                       // urls.get(note.getDomain() + " " + note.getTitle().trim()).add(email);
                                    }
                                }
                                channel.basicPublish(outputExchangeName, outputQueueName, MessageProperties.TEXT_PLAIN, gson.toJson(note).getBytes("UTF-8"));
                                log.info("deliveryTag " + deliveryTag);
                            }
                        }
                    }
                    channel.basicAck(deliveryTag, false);
                } catch (UnsupportedEncodingException ex) {
                    log.error("Exception: " + ex);
                } catch (IOException | ClassNotFoundException ex) {
                    log.error("Exception: " + ex);
                } catch (SQLException ex) {
                    log.error("Exception: " + ex);
                    ex.printStackTrace();
                }
            }
        });
    }
}
