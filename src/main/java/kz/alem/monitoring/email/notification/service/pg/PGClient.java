package kz.alem.monitoring.email.notification.service.pg;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kz.alem.monitoring.email.notification.service.config.AppConfig;
import kz.alem.monitoring.email.notification.service.model.Topic;
import org.apache.log4j.Logger;

/**
 * Class that responsible for the connection to postgre
 * and retrieving topics which corresponds to users from topic_notification table
 * @author ainur
 */

public class PGClient {

    private final Logger log = Logger.getLogger(PGClient.class);
    private final AppConfig appConfig;
    private Connection conn = null;
    private Statement stmt = null;
//    private final String sql = "select t.id topicid, t.name topicname,t.geo_position geoposition, u.email email, tn.email_notification_time notification_time, tn.sentiment sentiment, tn.media_type media_type \n"
//            + "from topics t, topic_user_rel tu, users u, topic_notification tn\n"
//            + "where tn.email_notification_time > 0 AND t.id = tn.topic_id AND u.id = tu.user_id AND tu.topic_id = t.id";
    
     private final String sql =
            "select tn.topic_id topicid, t.name topicname,t.geo_position geoposition,  tn.sentiment sentiment, tn.media_type media_type, tar.scopes scope,tn.chat_id as chats\n" + 
            "from topics t, user_chat_telegram  tn, tarif tar, users u, companies c\n"+
            "where  t.id = tn.topic_id AND tn.user_id=u.id AND u.company_id = c.id AND c.tarif_id=tar.id";
//"select tn.topic_id topicid, t.name topicname,t.geo_position geoposition,  tn.email_notification_time notification_time, tn.sentiment sentiment, tn.media_type media_type, tar.scopes scope, string_agg(tele.chat_id ::varchar, ',')  chats\n" +
//"from topics t, topic_notification tn, tarif tar, users u, companies c , user_chat_telegram tele\n" +
//"where tn.email_notification_time > 0 AND t.id = tn.topic_id AND tn.user_id=u.id AND u.company_id = c.id AND c.tarif_id=tar.id\n" +
//"and  tele.user_id =  tn.user_id\n" +
//"GROUP BY (topicid , topicname, geoposition, notification_time, sentiment,media_type, scope)";

    public PGClient() throws ClassNotFoundException, SQLException, IOException {
        appConfig = new AppConfig();
    }

    public List<Topic> getTopics() throws SQLException, ClassNotFoundException {
        this.connect();
        List<Topic> topics = new ArrayList<>();
        stmt = conn.createStatement();
        try (ResultSet rs = stmt.executeQuery(sql)) {
            while (rs.next()) {
                String topicId = rs.getString("topicid");
                String email = rs.getString("chats");
                String topicName = rs.getString("topicname");
                String sentiment = rs.getString("sentiment");
                String mediaType = rs.getString("media_type");
                String geoposition = rs.getString("geoposition");
                String scope = rs.getString("scope");
              //  String chatId  =  rs.getString("chat_id");
                if (!topicId.equals("") && !email.equals("") && !topicName.equals("") && !sentiment.equals("") && !mediaType.equals("")
                        && topicId != null && email != null && topicName != null && sentiment != null && mediaType != null) {
                    List<String> emails = Arrays.asList(email.split(","));
                  //  List<String> chatIds  =   Arrays.asList(chatId.split(","));
                    List<String> sentiments = Arrays.asList(sentiment.split(","));
                    List<String> mediaTypes = new ArrayList<>(Arrays.asList(mediaType.split(",")));
                    if (mediaTypes.contains("mass_media")) {
                        mediaTypes.add("print");
                        mediaTypes.add("news_agency");
                        mediaTypes.add("internet");
                        mediaTypes.add("tv");
                    }
                    if (mediaTypes.contains("social")) {
                        mediaTypes.add("facebook");
                        mediaTypes.add("instagram");
                        mediaTypes.add("vk");
                        mediaTypes.add("twitter");
                    }
                    Topic topic = new Topic();
                    topic.setTopicId(topicId);
                    topic.setTopicName(topicName);
                    topic.setSentiments(sentiments);
                    topic.setMediaTypes(mediaTypes);
                    topic.setGeoposotion(geoposition);
                    topic.setScope(scope);
                    topic.setChats(emails);
                    topics.add(topic);
                }
            }
        } catch (SQLException ex) {
            log.error("Statement doesn't executable ", ex);
        } finally {
            stmt.close();
            conn.close();
        }
        return topics;
    }
    private void connect() throws ClassNotFoundException, SQLException {
        Class.forName(appConfig.getClassName());
        conn = DriverManager.getConnection(appConfig.getUrl(), appConfig.getUser(), appConfig.getPass());
    }
}
