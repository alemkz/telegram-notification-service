package kz.alem.monitoring.email.notification.service.config;

import java.io.IOException;
import java.util.Properties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
/**
 * 
 * @author Ainur
 * Configuration class that responsible to set
 * configuration properties for rabbit and jdbc
 */
public class AppConfig {

    private final String user;
    private final String pass;
    private final String url;
    private final String className;
    private final String inputQueueName;
    private final String inputExchangeName;
    private final String outputQueueName;
    private final String outputExchangeName;
    private final String inputBindingKey;
    private final String rabbitHost;
    private final String rabbitUser;
    private final String rabbitPassword;

    public AppConfig() throws IOException {
        Resource resource = new ClassPathResource("/config.properties");
        Properties props = PropertiesLoaderUtils.loadProperties(resource);

        //jdbc config
        url = props.getProperty("jdbc.url");
        user = props.getProperty("jdbc.username");
        pass = props.getProperty("jdbc.password");
        className = props.getProperty("jdbc.driverClassName");
        
        //rabbitmq config
        inputQueueName = props.getProperty("input.queue");
        inputExchangeName = props.getProperty("input.exchange");
        outputQueueName = props.getProperty("output.queue");
        outputExchangeName = props.getProperty("output.exchange");
        inputBindingKey = props.getProperty("input.binding.key");
        rabbitHost = props.getProperty("rabbitmq.host");
        rabbitUser = props.getProperty("rabbitmq.user");
        rabbitPassword = props.getProperty("rabbitmq.password");
    }
    
    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

    public String getUrl() {
        return url;
    }

    public String getClassName() {
        return className;
    }
    
    public String getInputQueueName() {
        return inputQueueName;
    }

    public String getInputExchangeName() {
        return inputExchangeName;
    }

    public String getOutputQueueName() {
        return outputQueueName;
    }

    public String getOutputExchangeName() {
        return outputExchangeName;
    }

     public String getInputBindingKey() {
        return inputBindingKey;
    }
     
    public String getRabbitHost() {
        return rabbitHost;
    }

    public String getRabbitUser() {
        return rabbitUser;
    }

    public String getRabbitPassword() {
        return rabbitPassword;
    }


}
