/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.alem.monitoring.email.notification.service.model;

import java.util.List;

public class Topic {

    private String topicId;
    private String topicName;
    private String geoposotion;
    private List<String> emails;
    private int time;
    private List<String> sentiments;
    private List<String> mediaTypes;
    private String scope;
    private List<String> chats;

    public List<String> getChats() {
        return chats;
    }

    public void setChats(List<String> chats) {
        this.chats = chats;
    }
     
    public String getTopicId() {
        return topicId;
    }

    /**
     * @param topicId the topicId to set
     */
    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    /**
     * @return the email
     */
    public List<String> getEmails() {
        return emails;
    }

    /**
     * @param email the email to set
     */
    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    /**
     * @return the time
     */
    public int getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(int time) {
        this.time = time;
    }

    /**
     * @return the topicName
     */
    public String getTopicName() {
        return topicName;
    }

    /**
     * @param topicName the topicName to set
     */
    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    /**
     * @return the sentiment
     */
    public List<String> getSentiments() {
        return sentiments;
    }

    /**
     * @param sentiment the sentiment to set
     */
    public void setSentiments(List<String> sentiments) {
        this.sentiments = sentiments;
    }

    /**
     * @return the mediaType
     */
    public List<String> getMediaTypes() {
        return mediaTypes;
    }

    /**
     * @param mediaType the mediaType to set
     */
    public void setMediaTypes(List<String> mediaTypes) {
        this.mediaTypes = mediaTypes;
    }

    /**
     * @return the geoposotion
     */
    public String getGeoposotion() {
        return geoposotion;  
    }

    /**
     * @param geoposotion the geoposotion to set
     */
    public void setGeoposotion(String geoposotion) {
        this.geoposotion = geoposotion;
    }
    /**
     * @return the scope
     */
    public String getScope() {
        return scope;
    }
    
    public void setScope(String scope) {
         this.scope = scope;
    }

}
