package kz.alem.monitoring.email.notification.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;
import org.apache.log4j.Logger;
import kz.alem.monitoring.email.notification.service.pg.PGClient;
import kz.alem.monitoring.email.notification.service.rabbitmq.RabbitClient;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
/**
 *
 * Main class runs project 
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
@ImportResource({"classpath:context.xml"})
public class Receiver {

    private static final Logger log = Logger.getLogger(Receiver.class);
    public static void main(String[] argv) {
        try {
            new Receiver().run();
        } catch (ClassNotFoundException ex) {
            log.error("The program can not run.");
            log.error(Receiver.class.getName()+" can not found");
        }
    }
    /**
     *
     *  The main method creates objects of RabbitClient and PGClient classes and
     *  invokes the consume method for RabbitClient's object 
     *  with parameter PGClient's object
     */
    public void run() throws ClassNotFoundException {
        try {
            RabbitClient rc = new RabbitClient();
            PGClient pgclient = new PGClient();
            rc.consume(pgclient);
            }
            catch (SQLException | IOException | TimeoutException ex) {
            log.error("Error message: ", ex);
            } 
    }
}
