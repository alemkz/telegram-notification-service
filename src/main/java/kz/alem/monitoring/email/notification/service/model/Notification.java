package kz.alem.monitoring.email.notification.service.model;

import java.util.Date;
import java.util.List;

public class Notification extends Topic {

    private List<String> highlight;
    private String url;
    private String title;
    private String domain;
    private Date datePublished;
    private Date dateCreated;
    private String sentiment;
    private String mediaType;
    
    public List<String> getHighlight() {
        return highlight;
    }

     public void setHighlight(List<String> highlight) {
        this.highlight = highlight;
    }

      public String getUrl() {
        return url;
    }

      public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    public void setDomain(String domain) {
        this.domain = domain;
    }
    
    public String getDomain() {
       return domain;
    }

    /**
     * @return the date
     */
    public Date getDatePublished() {
        return datePublished;
    }

    /**
     * @param datePablished the datePablished to set
     */
    public void setDatePublished(Date datePublished) {
        this.datePublished = datePublished;
    }

    /**
     * @return the sentiment
     */
    public String getSentiment() {
        return sentiment;
    }

    /**
     * @param sentiment the sentiment to set
     */
    public void setSentiment(String sentiment) {
        this.sentiment = sentiment;
    }

    /**
     * @return the mediaType
     */
    public String getMediaType() {
        return mediaType;
    }

    /**
     * @param mediaType the mediaType to set
     */
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }
    
    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

}
